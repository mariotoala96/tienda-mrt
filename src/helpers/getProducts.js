import axios from 'axios';

export const getProducts = async() => {

    const url = `https://dummyjson.com/products`;
    const resp = await axios.get(url);
    const { products } = resp.data;

    const productos = products.map(producto => ({
        id: producto.id,
        imagen: producto.thumbnail,
        nombre: producto.title,
        precio: producto.price,
    }))

    return productos;
}