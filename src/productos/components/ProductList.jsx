import { getProducts } from "../../helpers/getProducts";
import { ProductCard } from "./ProductCard";
import { useEffect, useState } from "react";

export const Producto = () => {
  const [productos, setProductos] = useState([]);

  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const productos = await getProducts();
        setProductos(productos);
      } catch (error) {
        console.error("Error al obtener los productos", error);
      }
    };

    fetchProducts();
  }, []);

  return (
    <div className="container">
      <div className="row row-cols-1 row-cols-sm-2 row-cols-md-4 g-3">
        {productos.map((producto) => (
          <ProductCard key={producto.id} {...producto} />
        ))}
      </div>
    </div>
  );
};
