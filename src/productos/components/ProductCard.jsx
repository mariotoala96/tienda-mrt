export const ProductCard = ({ id, imagen, nombre, precio }) => {
  const ImageUrl = imagen;
  return (
    <div className="col">
      <div className="card shadow-sm">
        <img
          src={ImageUrl}
          className="bd-placeholder-img card-img-top image-product"
          alt={nombre}
        />
        <div className="card-body">
          <p className="card-text title-product text-truncate">{nombre}</p>
          <p className="card-text price-product">${precio}</p>

          <div className="d-grid gap-2">
            <button type="button" className="btn btn-sm btn-block btn-dark">
              Agregar
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
