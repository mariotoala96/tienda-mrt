import { Link } from 'react-router-dom';

export const Navbar = () => {

  return (
    <nav className="navbar navbar-expand-sm navbar-dark bg-dark p-2">
      <Link className="navbar-brand" to="/">
        <img src="img/react.svg" alt="logo" className='icon-react'/>
        MyEcommence
      </Link>

      <div className="navbar-collapse collapse w-100 order-3 dual-collapse2 d-flex justify-content-end">
        <ul className="navbar-nav ml-auto">
          <span className="nav-item nav-link text-primary">Mario Toala</span>
          <button className="nav-item nav-link btn btn-auth" >
            Logout
          </button>
        </ul>
      </div>
    </nav>
  );
};
  