import { Route, Routes } from "react-router-dom";
import { Devices } from "../productos/pages/Devices";
import { Navbar} from '../ui/component/Navbar';
import { FooterComponent } from "../ui/component/FooterComponent";



export const AppRouter = () => {
  return (
    <>
     <Navbar />
      <Routes>
        <Route path="/*" element={<Devices />} />
      </Routes>
      <FooterComponent />
    </>
  );
};
